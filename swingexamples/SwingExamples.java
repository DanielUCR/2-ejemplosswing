package swingexamples;

import Interface.DynamicWindow;
import Interface.Drawings;
import Interface.GraphicsPOO;
import Interface.MDI;
import Interface.RandomGraphics;
import Interface.Simple;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class SwingExamples {
    
    /*
    *
    *@Author Newville
    *
    */
    
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        try{
            int option = Integer.parseInt(JOptionPane.showInputDialog(null,"1-Simple\n2-MDI\n3-Dynamic Window\n4-Graphics\n5-Graphics POO\n6-Random Graphics"));
            switch (option){
                case 1:
                    frame.add(new Simple());
                    init(frame);
                    break;
                case 2:
                    frame.add(new MDI());
                    init(frame);
                    break;
                case 3:
                    frame.add(new DynamicWindow());
                    init(frame);
                    break;
                case 4:
                    frame.add(new Drawings());
                    init(frame);
                    break;
                case 5:
                    frame.add(new GraphicsPOO());
                    init(frame);
                    break;
                case 6:
                    frame.add(new RandomGraphics());
                    init(frame);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "U broke it");
                    break;
            }
        }
        catch (NumberFormatException ex){
            JOptionPane.showMessageDialog(null, "U broke it");
        }
    }
    
    public static void init(JFrame frame) {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
