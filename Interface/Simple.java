package Interface;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Simple extends JPanel implements ActionListener{

    private JTextField nameField;
    private JButton button;
    private JLabel nameLabel;
    
    public Simple(){
        this.setLayout(null);
        this.setPreferredSize(new Dimension(400,300));
        
        this.nameLabel = new JLabel("Enter your name:");
        this.nameField = new JTextField();
        this.button = new JButton("Click me");
        
        this.nameLabel.setLocation(150, 100);
        this.nameField.setLocation(100, 125);
        this.button.setLocation(150, 150);
        
        this.nameLabel.setSize(100, 20);
        this.nameField.setSize(200, 20);
        this.button.setSize(100, 25);
        
        this.button.addActionListener(this);
        
        this.add(this.nameLabel);
        this.add(this.nameField);
        this.add(this.button);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.button){
            if(this.nameField.getText().equals("")){
                JOptionPane.showMessageDialog(this, "Please enter your name");
            }
            else{
                JOptionPane.showMessageDialog(this, "Hello " + this.nameField.getText());
            }
        }
    }
}