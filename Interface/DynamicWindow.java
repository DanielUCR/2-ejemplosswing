package Interface;

import Utility.AmountException;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class DynamicWindow extends JPanel implements ActionListener{

    private ArrayList<JLabel> labelList;
    private ArrayList<JTextField> fieldList;
    private JTextField amountField;
    private JLabel amountLabel;
    private JButton generate;
    private JButton sumAll;
    
    public DynamicWindow(){
        this.setLayout(null);
        this.setPreferredSize(new Dimension(800,600));
        
        this.fieldList = new ArrayList<JTextField>();
        this.labelList = new ArrayList<JLabel>();
        this.amountLabel = new JLabel("Amount");
        this.amountField = new JTextField();
        this.generate = new JButton("Generate");
        this.sumAll = new JButton("Sum all");
        
        this.amountLabel.setBounds(25, 25, 150, 20);
        this.amountField.setBounds(25, 50, 75, 20);
        this.generate.setBounds(150, 50, 100, 20);
        this.generate.addActionListener(this);
        this.sumAll.addActionListener(this);
        
        this.add(this.generate);
        this.add(this.amountField);
        this.add(this.amountLabel);
    }

    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == this.generate){
            try{
                int amount = Integer.parseInt(this.amountField.getText());
                if(amount <= 0 || amount > 7){
                    throw new AmountException();
                }
                for(int i = 0; i < amount; i++){
                    this.labelList.add(new JLabel("Number " + (i+1)));
                    this.fieldList.add(new JTextField());
                    
                    this.labelList.get(i).setSize(100, 20);
                    this.fieldList.get(i).setSize(100, 20);
                    
                    this.labelList.get(i).setLocation(25, (150 + i*50));
                    this.fieldList.get(i).setLocation(25, (170 + i*50));
                    
                    this.add(this.labelList.get(i));
                    this.add(this.fieldList.get(i));
                    
                    if(i == amount-1){
                        this.sumAll.setLocation(25, 200+i*50);
                        this.sumAll.setSize(150, 25);
                        this.add(this.sumAll);
                        this.generate.setEnabled(false);
                        this.repaint();
                    }
                }
            }
            catch(AmountException ex){
                JOptionPane.showMessageDialog(this, "Number not in range - use only 1 to 7");
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(this, "Please use only numbers");
            }
        }
        else if(e.getSource() == this.sumAll){
            try{
                int sum = 0;
                for(JTextField temp : this.fieldList){
                    sum += Integer.parseInt(temp.getText());
                    JOptionPane.showMessageDialog(this, "The sum of all numbers is: " + sum);
                }
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(this, "Please use only numbers");
            }
        }
    }
}