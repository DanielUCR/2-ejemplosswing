package Interface;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

public class RandomGraphics extends JPanel implements Runnable {

    private Thread thread;

    public RandomGraphics() {
        this.setLayout(null);
        this.setPreferredSize(new Dimension(800, 600));
        this.thread = new Thread(this);
        this.thread.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Random rand = new Random();
        g.setColor(Color.red);
        g.fillRect(rand.nextInt(700) + 1, rand.nextInt(500) + 1, 100, 100);
        g.setColor(Color.blue);
        g.fillOval(rand.nextInt(700) + 1, rand.nextInt(500) + 1, 100, 100);
    }
    
    @Override
    public void run() {
        while(true){
            try {
                this.thread.sleep(500);
                repaint();
            } 
            catch (InterruptedException ex) {}
        }
    }
}