package Interface;

import Domain.Square;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

public class GraphicsPOO extends JPanel implements ActionListener{

    private JButton bigger;
    private JButton smaller;
    private Square square;
    
    public GraphicsPOO(){
        this.setLayout(null);
        this.setPreferredSize(new Dimension(800,600));
        this.square = new Square(300, 200, 100, 100);
        
        this.bigger = new JButton("Bigger!!");
        this.smaller = new JButton("Smaller!!");
        
        this.bigger.setBounds(25, 25, 100, 25);
        this.smaller.setBounds(150, 25, 100, 25);
        this.bigger.addActionListener(this);
        this.smaller.addActionListener(this);
        
        this.add(this.smaller);
        this.add(this.bigger);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.fillRect(this.square.getX(), this.square.getY(), this.square.getHeight(), this.square.getWidth());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.bigger){
            if(square.getHeight() > 400){
                return;
            }
            square.setX(square.getX()-10);
            square.setY(square.getY()-10);
            square.setHeight(square.getHeight()+25);
            square.setWidth(square.getWidth()+25);
            repaint();
        }
        else{
            if(square.getHeight() < 100){
                return;
            }
            square.setX(square.getX()+10);
            square.setY(square.getY()+10);
            square.setHeight(square.getHeight()-25);
            square.setWidth(square.getWidth()-25);
            repaint();
        }
    }
}