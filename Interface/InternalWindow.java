package Interface;

import java.awt.Dimension;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

public class InternalWindow extends JInternalFrame{

    private final int WIDTH = 400;
    private final int HEIGHT = 300;
    private JLabel label;
    
    public InternalWindow(){
        this.setLayout(null);
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        this.setClosable(true);
        this.setResizable(false);
        
        this.label = new JLabel("This is an MDI example");
        this.label.setLocation(WIDTH/2-50,HEIGHT/2-50);
        this.label.setSize(150, 20);
        this.setVisible(true);
        this.pack();
        this.add(this.label);
    }
}
