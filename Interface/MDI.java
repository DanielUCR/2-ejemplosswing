package Interface;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDesktopPane;

public class MDI extends JDesktopPane implements ActionListener{

    private JButton button;
    private InternalWindow internalWindow;
    
    public MDI(){
        this.setLayout(null);
        this.setPreferredSize(new Dimension(800, 600));
        this.button = new JButton("Click me");
        this.internalWindow = new InternalWindow();
        
        this.button.setLocation(150, 150);
        this.button.setSize(150, 75);
        
        this.button.addActionListener(this);
        this.add(this.button);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.button){
            if(!this.internalWindow.isShowing()){
                this.internalWindow = new InternalWindow();
                this.add(this.internalWindow);
                this.internalWindow.toFront();
            }
        }
    }
}
