package Interface;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JPanel;

public class Drawings extends JPanel{
    
    public Drawings(){
        this.setLayout(null);
        this.setPreferredSize(new Dimension(800,600));
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.drawLine(25, 25, 125, 25);
        g.drawRect(25, 50, 100, 100);
        g.drawOval(25, 200, 100, 100);
        g.setColor(Color.red);
        g.fillRect(25, 350, 100, 100);
        g.fillOval(25, 500, 100, 100);
       
        g.setColor(Color.lightGray);
        Polygon polygon = new Polygon();
        polygon.addPoint(200, 25);
        polygon.addPoint(225, 50);
        polygon.addPoint(275, 60);
        polygon.addPoint(225, 70);
        polygon.addPoint(250, 100);
        polygon.addPoint(200, 75);
        g.fillPolygon(polygon);
    }
}